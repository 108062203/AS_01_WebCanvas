# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.
    
    ![](https://i.imgur.com/DhXkCyr.png)
    Painter:
    After clicking the button, you can paint on the canvas by clicking and moveing your mouse on the canvas.
    
    ![](https://i.imgur.com/4pDF2UJ.png)
    Eraser:
    After clicking the button, you can erase anything on the canvas by clicking and moveing your mouse on the canvas.
    
    ![](https://i.imgur.com/K0jZYjK.png)
    TextInput:
    After clicking the button, you can type some text on the canvas by clicking the canvas and pressing keyboards.
    If you didn't press 'ENTER', it will not allow you to type another text on the canvas.
    
    ![](https://i.imgur.com/JnCoxAr.png)
    Brush shapes:
    After clicking the buttons, you can draw the shape chosen on the canvas by clicking and moveing your mouse on the canvas.
    
    ![](https://i.imgur.com/h4L12Cz.png)
    Undo and Redo:
    After clicking the buttons, you can reset the canvas, back to the previous step, back to the next step respectly.
    
    ![](https://i.imgur.com/Vn0Icmh.png)
    Download and Upload:
    After clicking the buttons, you can download the image on the canvas and upload new image on the canvas respectly.
    
    ![](https://i.imgur.com/nb6QFOc.png)
    Brush size:
    You can change the width of the tool chosen by clicking and moveing the point, or just clicking on the line.
    
    ![](https://i.imgur.com/yzS6ZPu.png)
    Colorpicker:
    You can change the color of the tool chosen by clicking the color on the site.
    
    ![](https://i.imgur.com/Ap8dGTs.png)
    You can change the font size or type by clicking.



### Function description

    Decribe your bouns function and how to use it.
    I didn't create any bouns function.

### Gitlab page link

    your web page URL, which should be "https://108062203.gitlab.io/AS_01_WebCanvas"

### Others (Optional)
    
    Anythinh you want to say to TAs.
    
    Nothing

<style>
table th{
    width: 100%;
}
</style>