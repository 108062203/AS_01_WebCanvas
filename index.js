const canvas = document.querySelector("#canvas");
const ctx = canvas.getContext("2d");
window.addEventListener("load", () => {


    canvas.width = window.innerWidth;
    canvas.height = 700;
    let painting = false;
    let erasing = false;
    let ispen = true;
    let iseraser = false;
    let istext = false;
    let iscircle = false;
    let isrect = false;
    let istrian = false;
    let circling = false;
    let recting = false;
    let trianing = false;
    let penbtn = document.getElementById("pen");
    let textbtn = document.getElementById("text");
    let eraserbtn = document.getElementById("eraser");
    let onclearbutton = document.getElementById("clear");
    let toolwidth = document.getElementById("pen-range");
    ctx.lineWidth = toolwidth.value;
    let imagedata = ctx.getImageData(canvas.offsetLeft,canvas.offsetTop,canvas.width,canvas.height);
    var nowfont = '12px sans-serif';
    var val = 'sans-serif';
    var size = '12px';
    var stack = [];
    var index = -1;
    var totalstack = 0;
    var hasInput = false;
    let rectbtn = document.getElementById("Rect");
    let circlebtn = document.getElementById("Circle");
    let trianbtn = document.getElementById("Trian");
    var ftselector = document.getElementById("fontselector");
    var szselector = document.getElementById("sizeselector");
    szselector.addEventListener("change", change_size);
    ftselector.addEventListener("change", change_font);
    document.getElementById("DOWNLOAD").addEventListener('click', download);
    canvas.onclick = function (e) {
        if(hasInput || !istext)return;
        addInput(e.offsetX, e.offsetY);
    }
    
    const reader = new FileReader();
    const img = new Image();
    
    const uploadImage = (e) => {
        reader.onload = () => {
            img.onload = () => {
                canvas.width = img.width;
                canvas.height = img.height;
                ctx.drawImage(img, 0, 0);
            };
            img.src = reader.result;
        };
        reader.readAsDataURL(e.target.files[0]);
    };
    
    function upload() {
        document.getElementById('labelupload').click();
      }

    function download() {
        const image = canvas.toDataURL();
        const link = document.createElement("a");
        link.href = image;
        link.download = "image.png";
        link.click();
    }
    const imageLoader = document.getElementById('uploader');
    imageLoader.addEventListener('change', uploadImage);

    function change_font() {
        let FtSel = document.getElementById("fontselector");
        val = FtSel.value;
        nowfont = size + " " + val;
        console.log(nowfont);
    }

    function change_size() {
        let SzSel = document.getElementById("sizeselector");
        size = SzSel.value;
        nowfont = size + " " + val;
        console.log(nowfont);
    }

    function addInput(x, y) {
        var input = document.createElement('input');

        input.type = 'text';
        input.style.position = 'fixed';
        input.style.left = (x - 4) + 'px';
        input.style.top = (y - 4) + 'px';

        input.onkeydown = headleEnter;

        document.body.appendChild(input);

        input.focus();

        hasInput = true;
    }

    function headleEnter(e) {
        var keyCode = e.keyCode;
        if(keyCode == 13){
            drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
            document.body.removeChild(this);
            hasInput = false;
        }
    }

    function drawText(txt, x, y) {
        ctx.textBaseline = 'top';
        ctx.textAlign = 'left';
        ctx.font = nowfont;
        ctx.fillText(txt, x-4, y-4);
    }
    var originx,originy;

    function startPosition(e){
        originx = e.offsetX;
        originy = e.offsetY;
        imagedata = ctx.getImageData(0, 0,canvas.width,canvas.height);
        if(totalstack!=0){
            totalstack--;
            index--;
            stack.pop();
        }
        stack.push(imagedata);
        totalstack++;
        index=totalstack-1;
        console.log("total =" + totalstack);
        console.log(index);
        ctx.lineWidth = toolwidth.value;
        if(ispen){
            ctx.globalCompositeOperation = "source-over";
            ctx.beginPath();
            painting = true;
            draw(e);
        }
        else if(iseraser){
            ctx.globalCompositeOperation = "destination-out";
            ctx.beginPath();
            erasing = true;
            erase(e);
        }
        else if(iscircle){
            ctx.globalCompositeOperation = "source-over";
            ctx.beginPath();
            circling = true;
            drawingcircle(e);
        }
        else if(isrect) {
            ctx.globalCompositeOperation = "source-over";
            ctx.beginPath();
            recting = true;
            drawingrect(e);
        }
        else if(istrian) {
            ctx.globalCompositeOperation = "source-over";
            ctx.beginPath();
            trianing = true;
            drawingtrian(e);
        }
    }
    
    function drawingcircle(e) {
        if(!circling) return;
        var radius = (Math.sqrt (Math.pow(e.offsetX - originx, 2) + Math.pow(e.offsetY - originy, 2)))/2;
        ctx.putImageData(imagedata, 0, 0);
        ctx.beginPath();
        ctx.arc((originx + e.offsetX) / 2, (originy + e.offsetY) / 2, radius, 0, 2 * Math.PI);
        ctx.stroke();
        console.log("circleing");
    }
    function drawingtrian(e) {
        if(!trianing) return;
        ctx.putImageData(imagedata, 0, 0);
        ctx.beginPath();
        ctx.moveTo((originx + e.offsetX) / 2, originy);
        ctx.lineTo(originx, e.offsetY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.closePath();
        ctx.stroke();
        console.log("trianing");
    }
    function drawingrect(e) {

        if(!recting) return;
        ctx.putImageData(imagedata, 0, 0);
        ctx.beginPath();
        ctx.moveTo(originx, originy);
        ctx.lineTo(originx, e.clientY);
        ctx.lineTo(e.clientX, e.clientY);
        ctx.lineTo(e.clientX, originy);
        ctx.closePath();

        ctx.stroke();
    }
    function endPosition(e){
        if(ispen)
            painting = false;
        else if(iseraser)
            erasing = false;
        else if(iscircle){
            circling = false;
            console.log("endcircle");
        }
        else if(isrect) {
            recting = false;
        }
        else if(istrian) {
            trianing = false;
        }

        stack.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        totalstack++;
        index++;
    }
    
    function draw(e){
        if(!painting) return;
        ctx.globalCompositeOperation = "source-over";
        ctx.lineWidth = toolwidth.value;
        ctx.lineCap = "round";
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(e.offsetX, e.offsetY);
    }
    

    function Undo() {
        if(totalstack == 0)return;
        if(index > 0){
            ctx.putImageData(stack[--index], 0, 0);
        }else
            ctx.putImageData(stack[index], 0, 0);
        console.log("total="+totalstack);
        console.log(index);
    }

    function Redo() {
        if(totalstack == 0)return;
        if(index < totalstack){
            if(index == totalstack-1){
                ctx.putImageData(stack[index], 0, 0);
            }else
            ctx.putImageData(stack[++index], 0, 0);
        }
        console.log("total="+totalstack);
        console.log(index);
    }
    function erase(e){
        if(!erasing) return;
        ctx.lineWidth = toolwidth.value;
        ctx.lineCap = "round";
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(e.offsetX, e.offsetY);
    }
    function Changetopen(){
        istext = false;
        iseraser = false;
        ispen = true;
        isrect = false;
        istrian = false;
        iscircle = false;
        canvas.style.cursor = "url('pencil.png'), auto";
        console.log("changetoopen");
    }
    
    function Changetoeraser(){
        istext = false;
        iseraser = true;
        ispen = false;
        isrect = false;
        istrian = false;
        iscircle = false;
        console.log("changetooeraser");
        canvas.style.cursor = "url('eraser.png'), auto";
    }

    function Changetotext() {
        istext = true;
        iseraser = false;
        ispen = false;
        isrect = false;
        istrian = false;
        iscircle = false;
        console.log("changetotextinput");
        canvas.style.cursor = "url('textcursor.png'), auto";
    }

    function Changetorect() {
        istext = false;
        iseraser = false;
        ispen = false;
        isrect = true;
        istrian = false;
        iscircle = false;
        console.log("changetorect")
        canvas.style.cursor = "url('rect.png'), auto";
    }

    function Changetocircle() {
        istext = false;
        iseraser = false;
        ispen = false;
        isrect = false;
        istrian = false;
        iscircle = true;
        console.log("changetocircle")
        canvas.style.cursor = "url('circle.png')20 20, auto";
    }

    function Changetotrian() {
        istext = false;
        iseraser = false;
        ispen = false;
        isrect = false;
        istrian = true;
        iscircle = false;
        canvas.style.cursor = "url('triangle.png'), auto";
        console.log("changetotrian")
    }

    function clear(){
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
    canvas.addEventListener("mousedown", startPosition);
    canvas.addEventListener("mouseup", endPosition);
    canvas.addEventListener("mousemove", draw);
    canvas.addEventListener("mousemove", erase);
    canvas.addEventListener("mousemove", drawingcircle);
    canvas.addEventListener("mousemove", drawingrect);
    canvas.addEventListener("mousemove", drawingtrian);
    penbtn.addEventListener("click", Changetopen);
    eraserbtn.addEventListener("click", Changetoeraser);
    textbtn.addEventListener("click", Changetotext);
    rectbtn.addEventListener("click", Changetorect);
    circlebtn.addEventListener("click", Changetocircle);
    trianbtn.addEventListener("click", Changetotrian);
    document.getElementById("undo").addEventListener("click", Undo);
    document.getElementById("redo").addEventListener("click", Redo);
    document.getElementById("uploadbtn").addEventListener("click", upload);
    

    onclearbutton.addEventListener("click", clear);

});

class Picker {
    constructor(target, width, height){
        this.target = target;
        this.width = width;
        this.height = height;
        this.target.width = width;
        this.target.height = height;
        //get context
        this.context = this.target.getContext("2d");
        this.pickerCircle = { x: 10, y: 10, width: 7, height: 7};
        this.listenForEvent();
    }

    fill() {
        this.build();   
    }
    build() {
        let gradient = this.context.createLinearGradient(0, 0, this.width, 0);
        gradient.addColorStop(0, "rgb(255, 0, 0)");
        gradient.addColorStop(0.15, "rgb(255, 0, 255)");
        gradient.addColorStop(0.33, "rgb(0, 0, 255)");
        gradient.addColorStop(0.49, "rgb(0, 255, 255)");
        gradient.addColorStop(0.67, "rgb(0, 255, 0)");
        gradient.addColorStop(0.84, "rgb(255, 255, 0)");
        gradient.addColorStop(1, "rgb(255, 0, 0)");
        
        this.context.fillStyle = gradient;
        this.context.fillRect(0, 0, this.width, this.height);

        gradient = this.context.createLinearGradient(0, 0, 0, this.height);
        gradient.addColorStop(0, "rgba(255, 255, 255, 1)");
        gradient.addColorStop(0.5, "rgba(255, 255, 255, 0)");
        gradient.addColorStop(0.5, "rgba(0, 0, 0, 0)");
        gradient.addColorStop(1, "rgba(0, 0, 0, 1)");
        this.context.fillStyle = gradient;
        this.context.fillRect(0, 0, this.width, this.height);

        this.context.beginPath();
        this.context.arc(this.pickerCircle.x, this.pickerCircle.y, this.pickerCircle.width, 0, Math.PI * 2);
        //this.context.strokeStyle = "black";
        this.context.stroke();
        this.context.closePath();
    }
    
    listenForEvent() {
        let isMouseDown = false;
        const onMouseDown = (e) => {
            let currentX = e.offsetX;
            let currentY = e.offsetY;
            if(currentY > this.pickerCircle.y && currentY < this.pickerCircle.y + this.pickerCircle.width && currentX > this.pickerCircle.x && currentX < this.pickerCircle.x + this.pickerCircle.width) {
                isMouseDown = true;
            }else {
                this.pickerCircle.x = currentX;
                this.pickerCircle.y = currentY;
            }
        }

        const onMouseMove = (e) => {
            if(isMouseDown) {
                let currentX = e.offsetX - this.target.offsetLeft;
                let currentY = e.offsetY - this.target.offsetTop;
                this.pickerCircle.x = currentX;
                this.pickerCircle.y = currentY;
            }
        }
        const ChangeColor = () => {
        this.onChangeCallback(this.getPickerColor());
    }
        
        const onMouseUp = () => {
            isMouseDown = false;
        }
        
        this.target.addEventListener("mousedown", onMouseDown);
        this.target.addEventListener("mousemove", onMouseMove);
        this.target.addEventListener("mousemove", ChangeColor);

        document.addEventListener("mouseup", onMouseUp);
    }

    getPickerColor() {

        let imageData = this.context.getImageData(this.pickerCircle.x, this.pickerCircle.y, 1, 1);

        ctx.strokeStyle = "rgb("+imageData.data[0]+","+imageData.data[1]+","+imageData.data[2]+")";  
        return { r: imageData.data[0], g:imageData.data[1], b:imageData.data[2]};

    } 

    onChange(callback) {
        this.onChangeCallback = callback;
    }
    
}

let picker = new Picker(document.getElementById("color-picker"),200,200);
setInterval(() => picker.fill(), 1);
picker.onChange((color) => {
    let selected = document.getElementsByClassName("selected")[0];
    selected.style.backgroundColor = `rgb(${color.r}, ${color.g}, ${color.b})`;
});
